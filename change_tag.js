(function($){
  "use strict";

  $.fn.changeTag = function(tagName, callback){

    return $(this.map(function(){
      var $newTag = $('<' + tagName + '/>');

      //copy all attributes
      $.each(this.attributes, function(_, attribute) {
        $newTag.attr(attribute.name, attribute.value);
      });

      //copy contents
      $newTag.append($(this).contents());
      
      //call callback passing the old element
      if (callback) {
        var newTag = callback.call($newTag, this);
        if (newTag instanceof jQuery) {
          $newTag = newTag;
        }
      }

      //clone events
      var events = $._data(this, "events");
      $.each(events, function(_, events) {
        $.each(events, function(_, event) {
          //Name spaces
          var eventType = event.type;
          if (event.namespace.length > 0) {
            eventType += '.' + event.namespace
          }
          $newTag.bind(eventType, event.data, event.handler);
        });
      });

      //replace old tag
      $(this).replaceWith($newTag);

      //return element to can create a jQuery object from new elements
      return $newTag[0];

    }));

  };

})(jQuery);
