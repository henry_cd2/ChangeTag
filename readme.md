changeTag
=========

A jQuery plugin to transform one tag into another whilst maintaining contents and attributes


Usage
-----

transforms all p tags into div tags

```javascript
$('p').changeTag('div');
```

A callback can be passed and will get called for every tag transformed. The callback gets passed the original element.

```javascript
$('p').changeTag('div', function(originalElem){
  $(this).addClass('transformed')
});
```

